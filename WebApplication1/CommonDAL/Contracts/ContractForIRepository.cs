﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.Interfaces;

namespace CommonDAL.Contracts
{
    [ContractClassFor(typeof(IRepository<>))]
    internal abstract class ContractForIRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        #region

        public void DeleteById(object id)
        {
            Contract.Requires<ArgumentNullException>(id != null);
        }

        public void Delete(TEntity entityToDelete)
        {
            Contract.Requires<ArgumentNullException>(entityToDelete != null);
        }

        public IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            Contract.Ensures(Contract.Result<IEnumerable<TEntity>>() != null);
            return null;
        }

        public TEntity GetById(object id)
        {
            Contract.Requires<ArgumentNullException>(id != null);
            return null;
        }

        public void Insert(TEntity entity)
        {
            Contract.Requires<ArgumentNullException>(entity != null);
        }

        public void Update(TEntity entityToUpdate)
        {
            Contract.Requires<ArgumentNullException>(entityToUpdate != null);
        }

        #endregion
    }
}
