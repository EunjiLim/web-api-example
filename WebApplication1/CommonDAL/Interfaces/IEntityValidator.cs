﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.Contracts;

namespace CommonDAL.Interfaces
{
    [ContractClass(typeof(ContractForIEntityValidator<>))]
    public interface IEntityValidator <in TEntity> where TEntity : class
    {
        void Validate(TEntity entity);
    }
}
