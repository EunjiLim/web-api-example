﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.Contracts;

namespace CommonDAL.Interfaces
{
    [ContractClass(typeof(ContractForIRepository<>))]
    public interface IRepository<TEntity> where TEntity : class
    {
        void DeleteById(object id);

        void Delete(TEntity entityToDelete);

        IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");

        TEntity GetById(object id);

        void Insert(TEntity entity);

        void Update(TEntity entityToUpdate);
    }
}
