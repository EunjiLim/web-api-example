﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEPAEntities;

namespace MockingEntityFrameworkUnitTest
{
    class UserSalesOrderDbSet : TestDbSet<SalesOrder>
    {
        public override SalesOrder Find(params object[] keyValues)
        {
            return this.SingleOrDefault(salesOrder => salesOrder.Id == (int) keyValues.Single());
        }
    }
}
