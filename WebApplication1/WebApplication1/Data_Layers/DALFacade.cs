﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonDAL.Interfaces;
using GEPAEntities;
using UnifiedDAL;
using WebApplication1.Models;

namespace WebApplication1.Data_Layers
{
    public static class DALFacade
    {
        public static IGEPAUnitOfWork CreateUnitOfWork()
        {
            return new DbUnitOfWork(new GEPAContext());
        }

        public static IEntityValidator<TEntity> GetValidator<TEntity>() where TEntity : class
        {
            return EntityValidatorFactory.Create<TEntity>();
        }
    }
}