﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonDAL;
using CommonDAL.Interfaces;
using UnifiedDAL;
using WebApplication1.Models;
using SalesOrder = GEPAEntities.SalesOrder;
using User = GEPAEntities.User;

namespace WebApplication1.Data_Layers
{
    public class DbUnitOfWork : IGEPAUnitOfWork
    {
        private readonly GEPAContext _context = new GEPAContext();
        private IRepository<User> _userRepository;
        private IRepository<SalesOrder> _salesOrdeRepository;

        public DbUnitOfWork() { }

        public DbUnitOfWork(GEPAContext context)
        {
            _context = context;
        }

        public void Save()
        {
            //
        }

        public IRepository<TEntity> GetRepository <TEntity>() where TEntity : class
        {
            var type = typeof(TEntity);

            if (type == typeof(User))
                return (IRepository<TEntity>)_userRepository;

            if (type == typeof(SalesOrder))
                return (IRepository<TEntity>) _salesOrdeRepository;

            throw new NotSupportedException(type.ToString());
        }

        public IRepository<User> UserRepository =>
            _userRepository ?? (_userRepository = new EFGenericRepository<GEPAContext, User>(_context));

        public IRepository<SalesOrder> SalesOrderRepository =>
            _salesOrdeRepository ?? (_salesOrdeRepository = new EFGenericRepository<GEPAContext, SalesOrder>(_context));
    }
}