﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using UnifiedDAL;

namespace WebApplication1.Models
{
    public class GEPAContext : BaseGEPAContext
    {
        public GEPAContext() : base("name=GEPAContext")
        {    
        }

        public GEPAContext(string connectionString) : base(connectionString)
        {
        }
    }
}