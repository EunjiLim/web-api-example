namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyContext : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SalesOrders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, unicode: false),
                        Password = c.String(nullable: false, unicode: false),
                        Role = c.Int(nullable: false),
                        Token = c.String(unicode: false),
                        TokenCreatedTime = c.DateTime(nullable: false, precision: 0),
                        TokenUpdatedTime = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SalesOrders", "UserId", "dbo.Users");
            DropIndex("dbo.SalesOrders", new[] { "UserId" });
            DropTable("dbo.Users");
            DropTable("dbo.SalesOrders");
        }
    }
}
