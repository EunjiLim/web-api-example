namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _new : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "UserId", c => c.String(nullable: false, maxLength: 12, storeType: "nvarchar"));
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false, maxLength: 16, storeType: "nvarchar"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false, unicode: false));
            AlterColumn("dbo.Users", "UserId", c => c.String(nullable: false, unicode: false));
        }
    }
}
