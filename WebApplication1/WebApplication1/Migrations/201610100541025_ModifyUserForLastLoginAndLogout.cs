namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyUserForLastLoginAndLogout : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "LastLoginTime", c => c.DateTime(precision: 0));
            AddColumn("dbo.Users", "LastLogoutTime", c => c.DateTime(precision: 0));
            DropColumn("dbo.Users", "Token");
            DropColumn("dbo.Users", "TokenCreatedTime");
            DropColumn("dbo.Users", "TokenUpdatedTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "TokenUpdatedTime", c => c.DateTime(precision: 0));
            AddColumn("dbo.Users", "TokenCreatedTime", c => c.DateTime(precision: 0));
            AddColumn("dbo.Users", "Token", c => c.String(unicode: false));
            DropColumn("dbo.Users", "LastLogoutTime");
            DropColumn("dbo.Users", "LastLoginTime");
        }
    }
}
