namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatetimeCanbeNull : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "TokenCreatedTime", c => c.DateTime(precision: 0));
            AlterColumn("dbo.Users", "TokenUpdatedTime", c => c.DateTime(precision: 0));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "TokenUpdatedTime", c => c.DateTime(nullable: false, precision: 0));
            AlterColumn("dbo.Users", "TokenCreatedTime", c => c.DateTime(nullable: false, precision: 0));
        }
    }
}
