﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using GEPAEntities;

namespace WebApplication1.Identity
{
    public class BasicAuthenticationIdentity : GenericIdentity
    {
        public string UserId { get; private set; }
        public string Password { get; private set; }
        public string Token { get; set; }
        public User User { get; set; }

        public BasicAuthenticationIdentity() : base("", "Basic")
        {
            
        }

        public BasicAuthenticationIdentity(string userId, string password)
            : base(userId, "Basic")
        {
            UserId = userId;
            Password = password;
        }
    }
}