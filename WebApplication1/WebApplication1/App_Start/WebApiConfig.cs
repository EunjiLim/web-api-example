﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using GEPAEntities;
using WebApplication1.Business_Logic.Filters;

namespace WebApplication1
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var builder = new ODataConventionModelBuilder();
            builder.ContainerName = "GEPAContainer";
            builder.EntitySet<User>("Users");
            builder.EntitySet<SalesOrder>("SalesOrders");
            builder.AddComplexType(typeof(UserRoleType));
            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());

            config.Filters.Add(new ExceptionFilter());
        }
    }
}
