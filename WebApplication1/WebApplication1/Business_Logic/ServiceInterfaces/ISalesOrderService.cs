﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEPAEntities;

namespace WebApplication1.Business_Logic.ServiceInterfaces
{
    public interface ISalesOrderService : IEntityService<SalesOrder>
    {
    }
}
