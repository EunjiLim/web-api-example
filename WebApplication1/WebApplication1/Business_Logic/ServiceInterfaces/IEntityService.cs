﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Business_Logic
{
    public interface IEntityService <TEntity> where TEntity : class
    {
        void Create(TEntity entity);

        void Delete(int key);

        TEntity GetById(int key);

        IEnumerable<TEntity> GetEntities();

        void Update(TEntity entity);
    }
}