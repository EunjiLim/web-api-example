﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using WebApplication1.Business_Logic.Services;
using WebApplication1.Identity;

namespace WebApplication1.Business_Logic.Filters
{
    public class AuthorizationRequiredFilter : ActionFilterAttribute
    {
        private const string Token = "Token";

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.Request.Headers.Contains(Token))
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                         {
                                             ReasonPhrase = "There is no token"
                                         };
                return;
            }

            var tokenValue = actionContext.Request.Headers.GetValues(Token).First();
            var user = new UserService().GetUserByToken(tokenValue);
            if (user == null)
            {
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized)
                                         {
                                             ReasonPhrase = "Invalid token"
                                         };
                return;
            }

            var basicAuthenticationIdentity = new BasicAuthenticationIdentity(user.UserId, user.Password)
                                              {
                                                  User = user
                                              };

            Thread.CurrentPrincipal = new GenericPrincipal(
                basicAuthenticationIdentity,
                new[]
                {
                    Convert.ToString(user.Role)
                });

            base.OnActionExecuting(actionContext);
        }
    }
}