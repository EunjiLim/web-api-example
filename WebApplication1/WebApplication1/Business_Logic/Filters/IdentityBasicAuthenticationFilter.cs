﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using WebApplication1.Business_Logic.ServiceInterfaces;
using WebApplication1.Business_Logic.Services;
using WebApplication1.Models;

namespace WebApplication1.Business_Logic.Filters
{
    public class IdentityBasicAuthenticationFilter : BasicAuthenticationFilter
    {
        protected override async Task<IPrincipal> AuthenticateAsync(
            string userId,
            string password,
            CancellationToken cancellationToken)
        {
            IUserService userService = new UserService();
            var user = userService.GetEntities().SingleOrDefault(x => x.UserId == userId && x.Password == password);
            if (user == null)
                return null;

            var identity = new ClaimsIdentity(null, null, "Basic", user.UserId, Convert.ToString(user.Role));
            return new ClaimsPrincipal(identity);
        }
    }
}