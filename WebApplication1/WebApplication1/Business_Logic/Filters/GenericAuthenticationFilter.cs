﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using GEPAEntities;
using WebApplication1.Business_Logic.Services;
using WebApplication1.Identity;

namespace WebApplication1.Business_Logic.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class GenericAuthenticationFilter : AuthorizationFilterAttribute
    {
        private readonly UserService _userService;

        public GenericAuthenticationFilter()
        {
            _userService = new UserService();
        }

        /// <summary>
        /// Checks basic authentication request
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            // Find user_id and password in request header
            var identity = FetchAuthHeader(filterContext);
            if (identity == null)
            {
                ChallengeAuthRequest(filterContext);
                return;
            }

            // Check the user exists or not who has the user_id and password
            var user = OnAuthorizeUser(identity.UserId, identity.Password, filterContext);
            if (user == null)
            {
                ChallengeAuthRequest(filterContext);
                return;
            }

            _userService.SetNewToken(user);
            // Assume(user.token != null)
            identity.Token = user.Token;

            var genericPrincipal = new GenericPrincipal(identity, new []{Convert.ToString(user.Role)});
            Thread.CurrentPrincipal = genericPrincipal;
            
            base.OnAuthorization(filterContext);
        }

        protected User OnAuthorizeUser(string userId, string password, HttpActionContext filterContext)
        {
            return _userService.GetEntities()
                               .SingleOrDefault(x => x.UserId == userId && x.Password == password);
        }

        /// <summary>
        /// Checks for autrhorization header in the request and parses it, creates user credentials and returns as BasicAuthenticationIdentity
        /// </summary>
        /// <param name="filterContext"></param>
        protected virtual BasicAuthenticationIdentity FetchAuthHeader(HttpActionContext filterContext)
        {

            AuthenticationHeaderValue authRequest = filterContext.Request.Headers.Authorization;
            string authHeaderValue = null;
            if (string.CompareOrdinal(authRequest?.Scheme, "Basic") != 0)
                authHeaderValue = authRequest.Parameter;

            if (string.IsNullOrEmpty(authHeaderValue))
                return null;

            authHeaderValue = Encoding.Default.GetString(Convert.FromBase64String(authHeaderValue));
            var credentials = authHeaderValue.Split(':');

            return credentials.Length < 2 ? null : new BasicAuthenticationIdentity(credentials[0], credentials[1]);
        }

        /// <summary>
        /// Send the Authentication Challenge request
        /// </summary>
        /// <param name="filterContext"></param>
        private static void ChallengeAuthRequest(HttpActionContext filterContext)
        {
            filterContext.Response = filterContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            filterContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", "http://localhost"));
        }
    }
}