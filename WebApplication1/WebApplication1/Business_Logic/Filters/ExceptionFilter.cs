﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace WebApplication1.Business_Logic.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is NotImplementedException)
                context.Response = new HttpResponseMessage(HttpStatusCode.NotImplemented);
            else if (context.Exception is ArgumentException || context.Exception is ArgumentNullException)
                context.Response = new HttpResponseMessage(HttpStatusCode.BadRequest) {};
    
            // 이런 것들 exception 발생할 때매다 찾아서 설정해주기.

        }
    }
}