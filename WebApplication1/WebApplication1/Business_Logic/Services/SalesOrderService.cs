﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CommonDAL.Interfaces;
using GEPAEntities;
using UnifiedDAL;
using WebApplication1.Business_Logic.ServiceInterfaces;
using WebApplication1.Data_Layers;

namespace WebApplication1.Business_Logic.Services
{
    public class SalesOrderService : ISalesOrderService
    {
        private readonly IGEPAUnitOfWork _unitOfWork; //= DALFacade.CreateUnitOfWork();
        private IEntityValidator<SalesOrder> _validator; // = DALFacade.GetValidator<User>();

        public SalesOrderService(IGEPAUnitOfWork unitOfWork, IEntityValidator<SalesOrder> validator)
        {
            _unitOfWork = unitOfWork;
            _validator = validator;
        }

        public void Create(SalesOrder entity)
        {
            _unitOfWork.SalesOrderRepository.Insert(entity);
            _unitOfWork.Save();
        }

        public void Delete(int key)
        {
            _unitOfWork.SalesOrderRepository.DeleteById(key);
            _unitOfWork.Save();
        }

        public SalesOrder GetById(int key)
        {
            return _unitOfWork.SalesOrderRepository.GetById(key);
        }

        public IEnumerable<SalesOrder> GetEntities()
        {
            return _unitOfWork.SalesOrderRepository.Get();
        }

        public void Update(SalesOrder entity)
        {
            _unitOfWork.SalesOrderRepository.Update(entity);
            _unitOfWork.Save();
        }
    }
}