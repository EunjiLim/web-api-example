﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.OData;
using CommonDAL.Interfaces;
using GEPAEntities;
using UnifiedDAL;
using WebApplication1.Business_Logic.ServiceInterfaces;
using WebApplication1.Data_Layers;

namespace WebApplication1.Business_Logic.Services
{
    public class UserService : IUserService
    {
        private readonly IGEPAUnitOfWork _unitOfWork; //= DALFacade.CreateUnitOfWork();
        private IEntityValidator<User> _validator; // = DALFacade.GetValidator<User>();

        public UserService(IGEPAUnitOfWork unitOfWork, IEntityValidator<User> validator)
        {
            _unitOfWork = unitOfWork;
            _validator = validator;
        }

        #region IEntityService

        public void Create(User entity)
        {
            _unitOfWork.UserRepository.Insert(entity);
            _unitOfWork.Save();
        }

        public void Delete([FromODataUri]int key)
        {
            _unitOfWork.UserRepository.DeleteById(key);
            _unitOfWork.Save();
        }

        public virtual User GetById(int key)
        {
            return _unitOfWork.UserRepository.GetById(key);
        }

        public IEnumerable<User> GetEntities()
        {
            return _unitOfWork.UserRepository.Get();
        }

        public void Update(User entity)
        {
            _unitOfWork.UserRepository.Update(entity);
            _unitOfWork.Save();
        }

        #endregion

        public IEnumerable<SalesOrder> GetSalesOrders(int userId)
        {
            return _unitOfWork.SalesOrderRepository.Get(x => x.UserId == userId);
        }

    }
}