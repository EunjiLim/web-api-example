﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GEPAEntities;
using UnifiedDAL;
using WebApplication1.Business_Logic.ServiceInterfaces;
using WebApplication1.Business_Logic.Services;

namespace WebApplication1.Business_Logic
{
    public static class BLFacade
    {
        private static readonly IGEPAUnitOfWork UnitOfWork =
            Data_Layers.DALFacade.CreateUnitOfWork();
        
        public static IUserService CreateUserService()
        {
            return new UserService(UnitOfWork, Data_Layers.DALFacade.GetValidator<User>());
        }

        public static ISalesOrderService CreateSalesOrderService()
        {
            return new SalesOrderService(UnitOfWork, Data_Layers.DALFacade.GetValidator<SalesOrder>());
        }
    }
}