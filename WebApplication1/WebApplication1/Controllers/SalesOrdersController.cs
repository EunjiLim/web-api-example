﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using GEPAEntities;
using WebApplication1.Business_Logic.ServiceInterfaces;
using WebApplication1.Business_Logic.Services;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using GEPAEntities;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<SalesOrder>("SalesOrders");
    builder.EntitySet<User>("Users"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class SalesOrdersController : ODataController
    {
        private readonly ISalesOrderService _service = new SalesOrderService();

        // GET: odata/SalesOrders
        [EnableQuery]
        public IQueryable<SalesOrder> GetSalesOrders()
        {
            return _service.GetEntities().AsQueryable();
        }

        // GET: odata/SalesOrders(5)
        [EnableQuery]
        public SingleResult<SalesOrder> GetSalesOrder([FromODataUri] int key)
        {
            return SingleResult.Create(
                new[]
                {
                    _service.GetById(key)
                }.AsQueryable());
        }

        // PUT: odata/SalesOrders(5)
        public IHttpActionResult Put([FromODataUri] int key, Delta<SalesOrder> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var entity = patch.GetEntity();

            try
            {
                _service.Update(entity);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

            return Updated(entity);
        }

        // POST: odata/SalesOrders
        public IHttpActionResult Post(SalesOrder salesOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _service.Create(salesOrder);
                return Created(salesOrder);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // PATCH: odata/SalesOrders(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<SalesOrder> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var entity = patch.GetEntity();

            try
            {
                _service.Update(entity);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

            return Updated(entity);
        }

        // DELETE: odata/SalesOrders(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            try
            {
                _service.Delete(key);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/SalesOrders(5)/User
        [EnableQuery]
        public SingleResult<User> GetUser([FromODataUri] int key)
        {
            return SingleResult.Create(_service.GetEntities().Where(x => x.Id == key).Select(x => x.User).AsQueryable());
        }

        protected override void Dispose(bool disposing)
        {
            //base.Dispose(disposing);
        }

        private bool SalesOrderExists(int key)
        {
            return _service.GetEntities().Count(x => x.Id == key) > 0;
        }
    }
}
