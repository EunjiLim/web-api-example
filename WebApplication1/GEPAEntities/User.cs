﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEPAEntities
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string UserId { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public UserRoleType Role { get; set; }

        public DateTime? LastLoginTime { get; set; }
        public DateTime? LastLogoutTime { get; set; }

        public virtual ICollection<SalesOrder> SalesrOders { get; set; }
    }
}
