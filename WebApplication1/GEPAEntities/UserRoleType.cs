﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEPAEntities
{
    public enum UserRoleType : int
    {
        Guest = 1,
        Operator = 2,
        Administrator = 3,
    }
}
