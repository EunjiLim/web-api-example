﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEPAEntities.Validators.Interfaces;

namespace GEPAEntities.Validators.Implementations
{
    internal class UserValidator : IUserValidator
    {
        public void Validate(User entity) { }

        public void ValidateUserId(string userId) { }

        public void ValidatePassword(string password) { }

        public void ValidateRole(int role) { }
    }
}
