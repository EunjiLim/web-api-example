﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.ValidatationException;
using GEPAEntities.Validators.Interfaces;

namespace GEPAEntities.Validators.Contracts
{
    [ContractClassFor(typeof(ISalesOrderValidator))]
    internal abstract class ContractForSalesOrderValidator : ISalesOrderValidator
    {
        public void Validate(SalesOrder entity)
        {
            ValidateUserId(entity.UserId);
        }

        public void ValidateUserId(int userId)
        {
            Contract.Requires<ValidationException>(userId > 0);
        }
    }
}
