﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.ValidatationException;
using GEPAEntities.Validators.Interfaces;

namespace GEPAEntities.Validators.Contracts
{
    [ContractClassFor(typeof(IUserValidator))]
    internal abstract class ContractForUserValidator : IUserValidator
    {
        public void Validate(User entity)
        {
            
        }

        public void ValidateUserId(string userId)
        {
            Contract.Requires<ValidationException>(!string.IsNullOrEmpty(userId));
            Contract.Requires<ValidationException>(!string.IsNullOrWhiteSpace(userId));
        }

        public void ValidatePassword(string password)
        {
            Contract.Requires<ValidationException>(!string.IsNullOrEmpty(password));
            Contract.Requires<ValidationException>(password.Length > 4);
        }

        public void ValidateRole(int role)
        {
            Contract.Requires<ValidationException>(role > 0 && role < 4);
        }
    }
}
