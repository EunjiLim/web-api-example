﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.Interfaces;
using GEPAEntities.Validators.Contracts;

namespace GEPAEntities.Validators.Interfaces
{
    [ContractClass(typeof(ContractForSalesOrderValidator))]
    public interface ISalesOrderValidator : IEntityValidator<SalesOrder>
    {
        void ValidateUserId(int userId);
    }
}
