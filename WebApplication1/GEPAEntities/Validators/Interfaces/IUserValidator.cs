﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.Interfaces;
using GEPAEntities.Validators.Contracts;

namespace GEPAEntities.Validators.Interfaces
{
    [ContractClass(typeof(ContractForUserValidator))]
    public interface IUserValidator : IEntityValidator<User>
    {
        void ValidateUserId(string userId);
        void ValidatePassword(string password);
        void ValidateRole(int role);
    }
}
