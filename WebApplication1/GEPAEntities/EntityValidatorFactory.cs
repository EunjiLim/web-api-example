﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.Interfaces;
using GEPAEntities.Validators.Implementations;

namespace GEPAEntities
{
    public class EntityValidatorFactory
    {
        public static IEntityValidator<TEntity> Create<TEntity>() where TEntity : class
        {
            var type = typeof(TEntity);

            if (type == typeof(User))
                return new UserValidator() as IEntityValidator<TEntity>;

            if (type == typeof(SalesOrder))
                return new SalesOrderValidator() as IEntityValidator<TEntity>;

            throw new NotSupportedException();
        }
    }
}
