﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CommonDAL.Interfaces;

namespace UnifiedDAL
{
    public class UnifiedRepository<TLocalEntity, TRemoteEntity> :
            IRepository<TLocalEntity>
        where TLocalEntity : class, new()
        where TRemoteEntity : class, new()
    {
        private readonly IRepository<TLocalEntity> _localRepository;

        private readonly IMapper _mapper;
        private readonly IRepository<TRemoteEntity> _remoteRepository;

        private readonly Action _saveForLocalEntities;
        private readonly Action _saveForRemoteEntities;

        public UnifiedRepository(
            IMapper mapper,
            IRepository<TLocalEntity> localRepository,
            IRepository<TRemoteEntity> remoteRepository,
            Action saveForLocalEntities,
            Action saveForRemoteEntities)
        {
            _mapper = mapper;
            _localRepository = localRepository;
            _remoteRepository = remoteRepository;
            _saveForLocalEntities = saveForLocalEntities;
            _saveForRemoteEntities = saveForRemoteEntities;
        }

        #region

        public void DeleteById(object id)
        {
            _remoteRepository.DeleteById(id);
            _saveForRemoteEntities();

            _localRepository.DeleteById(id);
        }

        public void Delete(TLocalEntity entityToDelete)
        {
            var remoteEntity = CopyAllProperties<TRemoteEntity>(entityToDelete);
            _remoteRepository.Delete(remoteEntity);
            _saveForRemoteEntities();

            _localRepository.Delete(entityToDelete);
        }

        public IEnumerable<TLocalEntity> Get(
            Expression<Func<TLocalEntity, bool>> filter = null,
            Func<IQueryable<TLocalEntity>, IOrderedQueryable<TLocalEntity>> orderBy = null,
            string includeProperties = "")
        {
            return _localRepository.Get(filter, orderBy, includeProperties);
        }

        public TLocalEntity GetById(object id)
        {
            var localEntity = _localRepository.GetById(id);
            if (localEntity != null)
                return localEntity;

            var remoteEntity = _remoteRepository.GetById(id);
            if (remoteEntity == null)
                return null;

            localEntity = CopyAllProperties<TLocalEntity>(remoteEntity);

            _localRepository.Insert(localEntity);
            _saveForLocalEntities();
            return _localRepository.GetById(id);
        }

        public void Insert(TLocalEntity localEntity)
        {
            var remoteEntity = CopyAllProperties<TRemoteEntity>(localEntity);

            _remoteRepository.Insert(remoteEntity);
            _saveForRemoteEntities();

            _mapper.Map(remoteEntity, localEntity);

            //localEntity = CopyAllProperties<TLocalEntity>(remoteEntity);
            _localRepository.Insert(localEntity);
        }

        public void Update(TLocalEntity entityToUpdate)
        {
            var remoteEntity = new TRemoteEntity();
            _mapper.Map(entityToUpdate, remoteEntity);

            _remoteRepository.Update(remoteEntity);
            _saveForRemoteEntities();

            _localRepository.Update(entityToUpdate);
        }

        #endregion

        public TTarget CopyAllProperties<TTarget>(object source) where TTarget : class
        {
            Contract.Assume(_mapper != null);
            return _mapper.Map<TTarget>(source);
        }
    }
}
