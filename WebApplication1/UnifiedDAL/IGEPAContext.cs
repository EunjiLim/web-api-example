﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEPAEntities;

namespace UnifiedDAL
{
    public interface IGEPAContext
    {
        DbSet<User> Users { get; set; }
        DbSet<SalesOrder> SalesOrders { get; set; }
    }
}
