﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonDAL.Interfaces;
using GEPAEntities;

namespace UnifiedDAL
{
    public interface IGEPAUnitOfWork : IUnitOfWork
    {
        IRepository<User> UserRepository { get; }
        IRepository<SalesOrder> SalesOrderRepository { get; }
    }
}
