﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GEPAEntities;

namespace UnifiedDAL
{
    public abstract class BaseGEPAContext : DbContext, IGEPAContext
    {
        protected BaseGEPAContext(string connectionString) : base(connectionString)
        {
        }

        #region IGEPAContext

        public DbSet<User> Users { get; set; }

        public DbSet<SalesOrder> SalesOrders { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var userEntity = modelBuilder.Entity<User>();
            userEntity.HasKey(x => x.Id);
            userEntity.Property(x => x.UserId).IsRequired().HasMaxLength(12);
            userEntity.Property(x => x.Password).IsRequired().HasMaxLength(16);
            
        }
    }
}
