﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IntegrationTestProject.OdataServiceReference;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace IntegrationTestProject
{
    [TestClass]
    public class UsersControllerTest
    {
        private GEPAContainer _container;

        [TestInitialize]
        public void Initialize()
        {
            Uri uri = new Uri("http://localhost:3393/odata");
            _container = new OdataServiceReference.GEPAContainer(uri);
        }

        [TestMethod]
        public void GetAllUsers()
        {
            // Act
            var users = _container.Users;

            // Assert
            Assert.IsNotNull(users);
        }

        [TestMethod]
        public void AddUsers()
        {
            // Arrange
            const int expectedStatusCode = 200;
            var user = new User()
                       {
                           UserId = "userId",
                           Password = "password",
                           Role = "Guest"
                       };

            // Act
            _container.AddToUsers(user);
            var serviceResponse = _container.SaveChanges();

            // Assert
            Assert.AreEqual(expectedStatusCode, serviceResponse.First().StatusCode);
        }
    }
}
